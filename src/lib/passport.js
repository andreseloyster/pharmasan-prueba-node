const passport = require('passport');
const LocalStrategy = require('passport-local');

const db = require('../database');
const helpers = require('../lib/helpers');
passport.use('local.login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, email, password, done) => {
    const rows = await db.query('SELECT * FROM users WHERE email = ?', [email]);
    if (rows.length > 0) {
        const user = rows[0];
        const validarPassword = await helpers.compararPassword (password, user.password);
        if (validarPassword) {
            done(null, user, req.flash('success','Bienvenido' + user.name));
        } else {
            done(null, false, req.flash('error','Password Incorrecto'));
        }
    } else {
        return done(null, false, req.flash('error','email no se encuentra registrado'));
    }
}));

passport.serializeUser((user, done) => {
    done(null, user.id)
});
passport.deserializeUser(async (id, done) => {
    const row = await db.query('SELECT * FROM users WHERE id = ?', [id]);
    done(null, row[0]);
});