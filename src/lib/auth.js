module.exports = {
    isLoggedIn(req,res,next){
        if(req.isAuthenticated()){
            return next();
        }
        return res.redirect('/login');
    },
    isNotLoggedIn(req,res,next){
        if(!req.isAuthenticated()){
            return next();
        }
        return res.redirect('/perfil');
    },
    administrador(req,res,next){
        if(req.isAuthenticated() && req.user.rol == 'ADMINISTRADOR'){
            console.log('entrando');
            return next();
        }
        return res.redirect('/acceso_denegado');

    }

}