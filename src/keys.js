module.exports ={
    database:{
        host:'localhost',
        port: 3306,
        user:'root',
        password:'',
        database:'pharmasan-laravel',
        schema: {
            tableName: 'sessions2',
            columnNames: {
                session_id: 'session_id',
                expires: 'expires',
                data: 'data'
            }
        }
    }
}