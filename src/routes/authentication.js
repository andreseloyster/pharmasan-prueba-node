const express = require('express');
const router = express.Router();

const passport = require('passport');

const { isLoggedIn , isNotLoggedIn }= require('../lib/auth')

router.get('/login',isNotLoggedIn, (req, res) => {
    res.render('auth/login')
});
router.post('/login', (req, res,next) => {
    passport.authenticate('local.login', {
        successRedirect: '/perfil',
        failureRedirect: '/login',
        failureFlash: true
    })(req,res,next)
});

router.get('/perfil',isLoggedIn, (req, res) => {
    res.render('perfil');
});
router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/login');
});
router.get('/acceso_denegado', (req, res) => {
    res.render('acceso_denegado');
});
module.exports = router;