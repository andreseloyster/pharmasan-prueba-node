const express = require('express');
const router = express.Router();
const db = require('../database');
const helpers = require('../lib/helpers');
const { isLoggedIn, administrador } = require('../lib/auth')
const excel = require("exceljs");
//clientes
router.get('/clientes', isLoggedIn, async (req, res) => {
    const clientes = await db.query('SELECT * FROM clientes');
    res.render('clientes/index', { clientes });
});
router.get('/crear_cliente', isLoggedIn, (req, res) => {
    res.render('clientes/crear');
});
router.post('/crear_cliente', isLoggedIn, async (req, res) => {
    const { name, document, email, address } = req.body;
    const objetoNuevo = {
        name,
        document,
        email,
        address
    };
    await db.query('INSERT INTO clientes set ?', [objetoNuevo]);
    req.flash('success', 'Operacion exitosa!');
    res.redirect('/pharmasan/clientes');
});
router.get('/editar_cliente/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const cliente = await db.query('SELECT * FROM clientes WHERE id = ?', [id]);
    res.render('clientes/editar', { cliente: cliente[0] });
});
router.post('/editar_cliente/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const { name, document, email, address } = req.body;
    const objetoNuevo = {
        name,
        document,
        email,
        address
    };
    await db.query('UPDATE clientes set ? WHERE id = ?', [objetoNuevo, id]);
    req.flash('success', 'Operacion exitosa!');
    res.redirect('/pharmasan/clientes');
});
router.get('/eliminar_cliente/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    await db.query('DELETE FROM clientes WHERE id = ?', [id]);
    req.flash('success', 'Operacion exitosa!');
    res.redirect('/pharmasan/clientes');
});

//usuarios
router.get('/usuarios', administrador, async (req, res) => {
    const usuarios = await db.query('SELECT * FROM users');
    res.render('usuarios/index', { usuarios });
});
router.get('/crear_usuario', administrador, (req, res) => {
    res.render('usuarios/crear');
});
router.post('/crear_usuario', administrador, async (req, res) => {
    const { name, email, password } = req.body;
    const objetoNuevo = {
        name,
        email,
        password
    };
    objetoNuevo.password = await helpers.encryptPassword(password);
    await db.query('INSERT INTO users set ?', [objetoNuevo]);
    req.flash('success', 'Operacion exitosa!');
    res.redirect('/pharmasan/usuarios');
});
router.get('/editar_usuario/:id', administrador, async (req, res) => {
    const { id } = req.params;
    const usuario = await db.query('SELECT * FROM users WHERE id = ?', [id]);
    res.render('usuarios/editar', { usuario: usuario[0] });
});
router.post('/editar_usuario/:id', administrador, async (req, res) => {
    const { id } = req.params;
    const { name, email, password } = req.body;
    if (password) {
        var objetoNuevo = {
            name,
            email,
            password
        };
        objetoNuevo.password = await helpers.encryptPassword(password);
    } else {
        var objetoNuevo = {
            name,
            email
        };
    }
    await db.query('UPDATE users set ? WHERE id = ?', [objetoNuevo, id]);
    req.flash('success', 'Operacion exitosa!');
    res.redirect('/pharmasan/usuarios');
});
router.get('/eliminar_usuario/:id', administrador, async (req, res) => {
    const { id } = req.params;
    await db.query('DELETE FROM users WHERE id = ?', [id]);
    req.flash('success', 'Operacion exitosa!');
    res.redirect('/pharmasan/usuarios');
});

//reporte
router.get('/reporte', isLoggedIn, async (req, res) => {
    const datos = await db.query('SELECT * FROM codigom ORDER BY ID ASC LIMIT 1000');
    res.render('reporte/index', { datos });
});
router.get('/exportar-excel', isLoggedIn, async (req, res) => {
    const datos = await db.query('SELECT * FROM codigom');
    let workbook = new excel.Workbook();
    let worksheet = workbook.addWorksheet("Reporte");

    worksheet.columns = [
        { header: "Id", key: "id", width: 5 },
        { header: "codigo", key: "codigo", width: 25 },
        { header: "descripcion", key: "descripcion", width: 25 },
        { header: "laboratorio", key: "laboratorio", width: 10 },
    ];

    worksheet.addRows(datos);

    res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
        "Content-Disposition",
        "attachment; filename=" + "tutorials.xlsx"
    );

    return workbook.xlsx.write(res).then(function () {
        res.status(200).end();
    });

});

module.exports = router;


